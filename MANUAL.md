---
title: "RuWEB: manual"
tags: ruweb, programación literaria, manual, dep:wc, dep:ls
---

:::info
Para la lectura de este documento se recomienda su [vista compartida](https://pad.programando.li/ruweb:manual?both).
:::

<div id="logo">🕷️</div>
<style>
  #logo {
    text-align: center;
    font-size: 10em !important;
  }
  h1 {
    text-align: center;
    margin-top: 0 !important;
  }
</style>

# RuWEB: manual

RuWEB es un [preprocesador](https://en.wikipedia.org/wiki/Preprocessor) de [programación literaria](http://www.literateprogramming.com/knuthweb.pdf) multilingüe y [colaborativo en tiempo cuasireal](https://en.wikipedia.org/wiki/Collaborative_real-time_editor) a partir de [pads](https://en.wikipedia.org/wiki/Notebook) con sintaxis [Markdown](https://es.wikipedia.org/wiki/Markdown). Es decir, RuWEB permite ejecutar textos en Markdown en cuyo proceso de escritura puede usarse una plataforma de pads como [Etherpad](https://etherpad.org), [HackMD](https://hackmd.io), [CodiMD](https://github.com/hackmdio/codimd), [HedgeDoc](https://hedgedoc.org) o [CryptPad](https://cryptpad.fr). RuWEB es el «estado del arte» de la unidad de trabajo [Programando LIBREros](https://programando.li/breros) en su búsqueda por publicar, desde una sola fuente y con _software_ libre o de código abierto ([FOSS](https://es.wikipedia.org/wiki/Software_libre_y_de_c%C3%B3digo_abierto), por sus siglas en inglés), una multiplicidad de formatos de manera automatizada. Esta punta de lanza de dicha unidad es una respuesta a las limitaciones técnicas que se nos empezaron a ser patentes con el pasar de los años.

Este pad es el manual de usuario de RuWEB. Si quieres saber más sobre cómo funciona y qué es el paradigma literario de programación propuesto por Knuth en 1984, visita [este otro pad](https://pad.programando.li/ruweb) con su fuente.

## Requisitos, instalación y actualización

RuWEB está hecha con [Ruby](https://www.ruby-lang.org) en al menos su versión 2.5.1, además de estar distribuida como una [gema](https://www.ruby-lang.org/es/libraries/). Para instalarla, solo ejecuta en tu [terminal](https://es.wikipedia.org/wiki/Emulador_de_terminal):

```sh x
gem install ruweb
```

Si no surge ningún inconveniente, puedes empezar a usar RuWEB si ejecutas:

```sh x
ruweb
```

Para actualizar RuWEB ejecutas:

```sh x
gem update ruweb
```

¡Eso es todo!

## Modos de uso

RuWEB trabaja sobre [URI](https://es.wikipedia.org/wiki/Identificador_de_recursos_uniforme) válidos como argumento. El recurso al que apunta la URI puede ser local o remoto, por lo que puedes usar RuWEB para archivos Markdown almacenados en tu dispositivo o disponibles públicamente a partir de una URL.

### Modo por defecto

En la modadlidad por defecto, la URI se especifica como último argumento. Por ejemplo, para ejecutar este pad sería:

```sh x
ruweb https://pad.programando.li/ruweb:manual/download
```

Si no especificaste una URI válida, RuWEB aborta la ejecución junto con el aviso:

```
🕷️ ruweb: read error from 'URI': run 'ruweb' for help
```

:::warning
<span id="about-url">La URL debe de apuntar al texto descargable en Markdown y no a su modo de edición</span>. En diversas plataformas solo tienes que tener en cuenta la coletilla de la URL. Por ejemplo:

* En Etherpad agregas `/export/txt`: `https://etherpad.wikimedia.org/p/PAD_ID/export/txt`
* En HackMD, CodiMD y HedgeDoc agregas `/download`: `https://docutopia.tupale.co/PAD_ID/download`
* En CryptPad no es posible descargar el Markdown desde una URL, por lo que tiene que hacerse de [manera manual](https://docs.cryptpad.fr/en/user_guide/apps/code.html#import-export) para después ejecutar el archivo con RuWEB.
:::

### Modo «cosido de fuente»

Por diseño, Markdown es una sintaxis fácil de procesar, leer y escribir. En seguimiento a esto, varias plataformas de escritura colaborativa que soportan esta sintaxis permiten visualizaciones minimalistas y legibles del texto. RuWEB sigue esta «filosofía», por lo que simplifica la ejecución del Markdown, así que por defecto RuWEB no guarda el pad como documento antes de su ejecución, sino que lo cose y ejecuta «al vuelo».

Acorde a Knuth, el primer proceso de su implementación de programación literaria lo denominó «_weaving the web_» («tejiendo la red»), con el cual se produce un documento de composición tipográfica legible por homínidos. RuWEB también cose el documento, pero para ejecutarlo directamente. Este proceso permite incorporar en una sola fuente la información de [una red de pads](#Red-de-pads).

Para guardar la fuente cosida como archivo en tu dispositivo usa el parámetro `--save-source` antes de la URI:

```sh x
ruweb --save-source https://pad.programando.li/ruweb:manual/download
```

Con el siguiente mensaje RuWEB te indica el nombre del archivo una vez que se ha guardado:

```
🕷️ ruweb: parameter detected: saving 'source.md' in 'URI'
```

Esta modalidad es útil si deseas descargar el pad cosido.

### Modo «cosido de código»

Knuth también habla de «_tangling the web_» («enmarañando la red») para describir el segundo y último proceso de su implementación de programación literaria con el cual se produce el código ejecutable por una máquina. En la implementación knuthiana la fuente es dividida en dos documentos, un texto tejido para un homínido y un programa enmarañado para una máquina. Este proceso permite obtener un orden de ejecución a partir de un criterio narrativo a través de la producción de dos archivos.

En RuWEB a la fuente cosida se le extrae el código para su ejecución. Este proceso permite obtener un orden de ejecución desde un criterio narrativo a través de la selección y la organización de bloques de código.

Para guardar el código cosido como archivo en tu dispositivo, en lugar de ejecutarlo, usa el parámetro `--save-code` antes de la URI:

```sh x
ruweb --save-code https://pad.programando.li/ruweb:manual/download
```

Con el siguiente mensaje RuWEB te indica el nombre del archivo una vez que se ha guardado:

```
🕷️ ruweb: parameter detected: saving 'code.txt' in 'URI'
```

Esta modalidad es útil si deseas descargar el código cosido para su depuración, auditoría o interoperatibilidad entre paradigmas.

### Modo «crudo»

Para guardar el pad tal cual como archivo en tu dispositivo usa el parámetro `--save-raw` antes de la URI:

```sh x
ruweb --save-raw https://pad.programando.li/ruweb:manual/download
```

Con el siguiente mensaje RuWEB te indica el nombre del archivo una vez que se ha guardado:

```
🕷️ ruweb: parameter detected: saving 'raw.md' in 'URI'
```

Esta modalidad es útil si deseas descargar el pad sin coser.

### Diversos modos

Todos los modos pueden utilizarse en conjunto y en cualquier orden, solo declara los parámetros antes de la URI. Por ejemplo:

```sh x
ruweb --save-source --save-code https://pad.programando.li/ruweb:manual/download
```

Otro ejemplo:

```sh x
ruweb --save-raw --save-source --save-code https://pad.programando.li/ruweb:manual/download
```

Con esto ya está todo listo para ver las diferentes maneras en como puedes escribir y ejecutar archivos de Markdown.

## Ejemplos tipos de uso

### Bloques de código

En Markdown, los bloques de código se declaran de la siguiente manera:

```
Un bloque de código
```

A los bloques es posible especificarles un lenguaje. Por ejemplo, aquí se declara el lengue Ruby:

```ruby
puts "[01/13] ✔️ Ejecución con Ruby"
```

Incluso es posible indicar números de línea con `=` para comenzar con el número 1, `=+` para tener continuidad en la numeración entre distintos bloques o `=d` donde $d$ equivale a cualquier número para comenzar la numeración:

```ruby=99
puts "[02/13] ✔️ Ejecución de nuevo con Ruby"
```

En la actualidad los números de línea no tienen efecto para alterar el orden de ejecución. Su indicación es solo para su lectura. Para alterarlo se usan [macros](#Declaración-y-llamado-de-macros) o [ejecuciones personalizadas](https://pad.programando.li/ruweb:manual?both#Ejecuciones-personalizadas)

### Bloques de código ejecutables y no ejecutables

Para que un bloque de código sea ejecutable, solo se necesita declarar su lenguaje. Por ejemplo:

```ruby=
puts "[03/13] ✔️ Ejecución una vez más con Ruby"
```

Si no se indicó un lenguaje, el código no es ejecutable. Por ejemplo:

```
Sin ejecutar
```

Si se indica un lenguaje, pero no se desea ejecutar, puede agregarse espacio + equis (`x`). Por ejemplo:

```ruby= x
puts "Esto tampoco se ejecuta"
```

Si se indicó el lenguaje pero este no es un comando existente o hubo un error en su ejecución, RuWEB imprime una excepción con el aviso:

```
🕷️ ruweb: something went wrong on top: saving ruweb_logN
```

Para fines de depuración, cuando esto sucede se guardan $N$ cantidad de registros por $N$ cantidad de excepciones en la ubicación actual de trabajo.

### Macros

Los macros permiten declarar bloques en cualquier orden usando la sintaxis `_` + cualquier caracter excepto espacios. Por ejemplo, así se llama al primer macro con el nombre `_tiempo_POSIX`:

```ruby=
def puts_timestamp
  _tiempo_POSIX
end
puts "[04/13] ✔️ Marca de tiempo: #{puts_timestamp}"
```

Los macros se declaran en el bloque de código después del nombre del lenguaje o del símbolo para el número de línea + espacio. Por ejemplo, esta sería la declaración del macro `_tiempo_POSIX`:

```ruby= _tiempo_POSIX
Time.now.to_i.to_s
```

La declaración de macros puede ser múltiple o anidada. Por ejemplo, esta es una declaración del macro `_día`:

```ruby= _día
Time.now.day
```

Ahora la declaración del macro`_mes`:

```ruby= _mes
[
  'enero',
  'febrero',
  'marzo',
  'abril',
  'mayo',
  'junio',
  'julio',
  'agosto',
  'septiembre',
  'octubre',
  'noviembre',
  'diciembre'
][Time.now.mon - 1]
```

El siguiente macro para `_fecha` anida a los macros `_día` y `_mes` en una sola línea:

```ruby= _fecha
def puts_date
  "#{_día} de #{_mes} del #{Time.now.year}"
end
```

Por fin se realiza la llamada a los macros:

```ruby=
_fecha
puts "[05/13] ✔️ Fecha: #{puts_date}"
```

El orden entre declaraciones de macros y sus llamadas es irrelevante para RuWEB. No obstante, la falta de declaración de algún macro resulta en un error de ejecución.

:::warning
El uso de multilínea en la declaración de macros podría provocar una excepción si el lenguaje utilizado no soporta la inyección de más de una línea. Al menos para el caso de Ruby no se ha detectado ningún inconveniente.
:::

### Multilingüismo

No se está constreñido a ejecutar un solo lenguaje en los bloques de código. Por ejemplo, además de Ruby se podría utilizar Bash:

```sh=
echo "[09/13] ✔️ Ejecución con Bash"
```

Cuando se emplean varios lenguajes, se concatenan los bloques de código del mismo lenguaje en una sola ejecución. Por ejemplo, se declara una variable en Ruby:

```ruby= 
concatenados = true
```

Ahora se usa lenguaje Bash entre bloques de Ruby:

```sh=
echo "[10/13] ✔️ De nuevo ejecutado con Bash"
```

Ahora se intenta de manera fallida imprimir la variable en Ruby:

```ruby= 
puts "[06/13] ✔️ ¿Están los bloques de Ruby concatenados? #{concatenados ? 'Sí' : 'No'}"
```

Los números entre corchetes permiten dar un poco más de claridad sobre la concatenación de bloques de un mismo lenguaje. Como antes de declarar bloques de Bash se declararon bloques de Ruby (hasta `[05/13]`), todos estos se concatenan para después dar paso a la concatenación de los bloques de Bash.

El orden de ejecución puede llegar a ser confuso porque en este paradigma de programación es posible escribir código en cualquier orden. Por ello, la recomendación es usar macros, más entendible para nosotros, antes que tratar de comprender el orden de ejecución que realiza tu dispositivo.

Si la concatenación no es deseable, es posible sobreescribir este comportamiento mediante [ejecuciones personalizadas](https://pad.programando.li/ruweb:manual?both#Ejecuciones-personalizadas).

### Ejecuciones personalizadas

RuWEB ejecutará una concatenación de bloques de código según el nombre de lenguaje especificado. Por ejemplo, para bloques escritos en Ruby el comando sería `ruby`. Sin embargo, hay nombres de lenguajes que no corresponden al comando para su ejecución o que requieren de argumentos adicionales. Por ejemplo, para exportar como imagen un diagrama de Graphviz no se usa `graphviz`, el lenguaje especificado para estos tipos de bloques, sino `dot` seguido de diversos argumentos.

Para poder sobreescribir este comportamiento, el comando se declara en el bloque de código después del nombre del lenguaje o de la declación del número de línea + espacio. Por ejemplo, para este bloque de Graphviz se usa `printf` y `wc` para imprimir la cantidad de líneas, palabras y caracteres del diagrama:

```graphviz printf "[11/13] ✔️ Ejecución de 'printf' y 'wc' de manera personalizada: " && wc
digraph diagrama {
  V -> w
}
```

Esto también puede permitir dos ejecuciones distintas de bloques de código escritos con el mismo lenguaje. Por ejemplo, a continuación se realizan dos ejecuciones independientes de Ruby:

```ruby=
una_variable = true
```

Para ejecutar otro proceso de Ruby basta con declarar una ejecución personalizada que vuelve a llamar a `ruby`. Para concluir con el ejemplo:

```ruby= ruby
puts "[12/13] ✔️ No debería estar definida 'una_variable', ¿lo está? #{defined?(una_variable) ? 'Sí' : 'No'}"
```

Por último, puede darse el caso de que el bloque de código requiere ser pasado en una posición específica de la ejecución personalizada. Para ello se emplea el símbolo `%` como indicador para la posición de su inserción, por ejemplo:

```sh= echo % | sed 's/^/[13\/13] ✔️ /'
"Ejecución personalizada y desde una posición específica"
```

:::warning
El uso de multilínea para `%` podría provocar una excepción.
:::

### Red de pads

En ocasiones es necesario establecer una red de pads debido a alguna de las siguientes circunstancias:

* Reutilización de pads para evitar la duplicación de datos.
* Solvencia ante el límite de caracteres de cada plataforma de escritura colaborativa.
* División de la información para mayor inteligibilidad.
* Segmentación de componentes en pos de implementaciones con mayor complejidad.

Entonces, para tener complementariedad es posible llamar a otro pad usando un guion bajo más el enlace en sintaxis Markdown con la URL al pad descargable. Por ejemplo, aquí se llama a 

:::info
▶️ [2022-05-29 02:34:39 -0500] [un pad con el comando `wc`](https://pad.programando.li/ruweb:cli:wc/download)
:::

:::info
Este pad sirve como prueba de concepto para la [implementación de otros pads](https://pad.programando.li/ruweb:manual#Implementaci%C3%B3n-de-otros-pads)  y como ejemplo de [implementación y declaración de macros](https://pad.programando.li/ruweb:manual?both#Implementaci%C3%B3n-y-declaraci%C3%B3n-de-macros) presentes en el [manual](https://pad.programando.li/ruweb:manual) de RuWEB.
:::

# Contador de palabras `wc`

## Requisitos

* Ruby > 2.5.1

## Opciones

* `print`: impresión en terminal; si `false`, regresa un `Int`.
* `uri`: [URI](https://es.wikipedia.org/wiki/Identificador_de_recursos_uniforme) al fichero.

Opciones por defecto:

```yaml _wc_config
print: false
uri: 'https://pad.programando.li/ruweb:manual/download'
```

## Módulo

```ruby=
module CLI
  extend self
  require 'open-uri'
  require 'yaml'

  def wc(config: YAML.load(%Q{_wc_config}))
    uri = config['uri']
    chars = File.exist?(uri) ? File.read(uri) : URI.parse(uri).open.read
    chars = chars.split(/\s+/)
    chars = chars.reject{ |char| char =~ /\b\W+\b/ }
    length = chars.length
    config['print'] ? puts(length) : length
  rescue
    puts "something went wrong with 'wc': check validity of '#{uri}'"
  end

end
```

:::info
⏹️ [2022-05-29 02:34:39 -0500] [un pad con el comando `wc`](https://pad.programando.li/ruweb:cli:wc/download)
:::

 para tener acceso a un método de Ruby que nos permite obtener el total de palabras de este pad:

```ruby=
puts "[07/13] ✔️ Total de palabras en el manual con método literario 'wc': #{CLI.wc}"
```

Como puedes observar, la llamada a pads no se hace dentro de bloques de código, sino en cualquier otro lugar del documento. Como cada pad es texto escrito en Markdown, también puede ser parte de la fuente que ejecuta RuWEB. Este preprocesador se encarga de coserlo en una sola fuente junto con el aviso:

```
🕷️ ruweb: other pad detected: sewing 'NOMBRE' from 'URI'
```

El resultado es la inserción de texto en el pad ejecutado. Para dejar patente este proceso, RuWEB inserta las cadenas de caracteres con esta estructura:

```
:::info
▶️ [FECHA] [NOMRE](URI)
:::

TEXTO DEL PAD LLAMADO SIN METADATOS YAML

:::info
⏹️ [FECHA] [NOMRE](URI)
:::
```

Si quieres corroborarlo, llama a RuWEB con el parámetro `--save-source` para cotejar el documento cosido con el nombre `source.md`:

```sh x
ruweb --save-source
```

La red de pads <span id="pad-network-enhancement">se potencia</span> si se usan macros después de su llamado. Por ejemplo, en 

:::info
▶️ [2022-05-29 02:34:39 -0500] [este pad con el comando `ls`](https://pad.programando.li/ruweb:cli:ls/download)
:::

# Lista de archivos `ls`

## Requisitos

* Ruby > 2.5.1

## Opciones

* `a`: impresión de ficheros ocultos.
* `A`: como `a` pero sin `.` ni `..`.
* `path`: ruta a enlistar.
* `print`: impresión en terminal; si `false`, regresa un `Array`.
* `short`: impresión de solo el nombre de los ficheros.

Opciones por defecto:

```yaml _ls_config
path: '.'
print: true
short: true
```

## Módulo

```ruby=
module CLI
  extend self
  require 'yaml'
  
  def ls(config: YAML.load(%Q{_ls_config}))
    path = "#{config['path']}/".gsub(/\/+/, '/')
    paths = ["#{path}*"]
    paths.push("#{path}.*") if config['A'] || config['a']
    files = Dir.glob(paths).sort
    files.reject! { |e| File.basename(e) =~ /^\.+$/} if config['A']
    files.map! { |e| File.basename(e) } if config['short']
    config['print'] ? puts(files) : files
  rescue
    puts "something went wrong with 'ls'"
  end
  
end
```

:::info
⏹️ [2022-05-29 02:34:39 -0500] [este pad con el comando `ls`](https://pad.programando.li/ruweb:cli:ls/download)
:::

 hay un macro `_ls_config` que a continuación se reescribe:

```yaml _ls_config
print: false
path: '/'
short: true
```

De esta manera los macros actúan a modo de parámetros de los pads llamados. Esto permite reutilizar pads con distintas configuraciones como a continuación se demuestra:

```ruby=
puts "[08/13] ✔️ Lista de archivos en el directorio '/' con comando emulado 'ls': #{CLI.ls.join(' ')}"
```

Ahora sí, ya está todo listo para que comiences a programar de manera literaria con RuWEB, ¡suerte!

## Casos de uso

Estos fueron unos ejemplos de cómo poder ejecutar Markdown gracias a la implementación de programación literaria de RuWEB. Para escenarios en el «mundo real» están los siguientes casos de uso que ¡tú también puedes ejecutar!

1. [Pecas: una metáfora de publicación](https://pad.programando.li/ruweb:pub:pecas). Este pad es un entorno reutilizable de publicación automatizada y la segunda implementación de Pecas.
2. Entradas de wiki. Estos pads son [how-to's](https://es.wikipedia.org/wiki/HOWTO) que documentan y ejecutan diversos procesos computacionales:
    1. [Respaldo de base de datos de HedgeDoc](https://pad.programando.li/ruweb:wiki:db-backup)
    2. [Consulta en base de datos de MariaDB](https://pad.programando.li/ruweb:wiki:mariadb)
    3. [Conexión como cliente a una instancia de Nebula](https://pad.programando.li/ruweb:wiki:nebula)
    4. [Descarga automatizada de libros de Virus Editorial](https://pad.programando.li/ruweb:wiki:descarga-virus-editorial)
3. Bash en Ruby. Estos pads emulan comandos de terminal en un módulo de Ruby:
    1. [Lista de archivos `ls`](https://pad.programando.li/ruweb:cli:ls)
    2. [Generador de directorios `mkdir`](https://pad.programando.li/ruweb:cli:mkdir)
    3. [Contador de palabras `wc`](https://pad.programando.li/ruweb:cli:wc)
4. Ruby extendido. Estos pads extienden funcionalidades de clases de Ruby.
    1. [Transliteración de cadenas](https://pad.programando.li/ruweb:rb:transliterate)

## Licencia

Este texto está bajo [Licencia Editorial Abierta y Libre (LEAL)](https://programando.li/bres). Con LEAL eres libre de usar, copiar, reeditar, modificar, distribuir o comercializar bajo las siguientes condiciones:

* Los productos derivados o modificados han de heredar algún tipo de LEAL.
* Los archivos editables y finales habrán de ser de acceso público.
* El contenido no puede implicar difamación, explotación o vigilancia.