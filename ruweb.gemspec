# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = 'ruweb'
  spec.version       = 1653809677.0
  spec.authors       = ['Programando LIBREros', 'Nika Zhenya']
  spec.email         = ['hi@programando.li']
  spec.summary       = 'Execute Markdown pads as literate programming.'
  spec.description   = 'Multilingual and collaborative literate programming in quasi-real time from pads.'
  spec.homepage      = 'https://pad.programando.li/ruweb'
  spec.license       = 'LEAL'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.5.1')

  raise 'RubyGems 2.0 or newer is required to protect against public gem pushes.' unless spec.respond_to?(:metadata)

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['allowed_push_host'] = 'https://rubygems.org/'
  spec.metadata['source_code_uri'] = 'https://gitlab.com/programando-libreros/herramientas/ruweb'
  spec.metadata['changelog_uri'] = 'https://pad.programando.li/ruweb#Lista-de-cambios'

  spec.files = ["README.md", "MANUAL.md", "raw/MANUAL.md", "raw/README.md", "CHANGELOG.md", "lib/ruweb/help.rb", "ruweb.gemspec", "lib/ruweb.rb", "lib/ruweb/sew.rb", "lib/ruweb/read.rb", "lib/ruweb/sewsource.rb", "lib/ruweb/sewcode.rb", "lib/ruweb/sewmacros.rb", "lib/ruweb/save.rb", "lib/ruweb/execute.rb", "exe/ruweb"]

  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
end