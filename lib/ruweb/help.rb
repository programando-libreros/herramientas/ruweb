# frozen_string_literal: true

module RuWEB
  # Displays help
  module Help
    extend self
    
    def init
      lang = ENV['LANG'] ? ENV['LANG'][0..1] : 'en'
      help = Help.method_defined?("help_in_#{lang}") ? "help_in_#{lang}" : 'help_in_en'
      puts send(help)
    end

    def help_in_es
      %Q{🕷️ ruweb: hola: mostrando ayuda

ruweb [OPCIÓN] [URI]

Opciones:
  --save-raw     Guarda el texto de la fuente sin modificar.
  --save-source  Guarda el texto de la fuente de toda la red de pads.
  --save-code    Guarda el texto del código en lugar de ejecutarlo.

Lee mi manual en 'https://pad.programando.li/ruweb:manual'.

Ejecutando ejemplo con mi manual:}
    end

    def help_in_en
      %Q{🕷️ ruweb: hi: displaying help

ruweb [OPTION] [URI]

Options:
  --save-raw     Saves source text unchanged.
  --save-source  Saves source text of the entire pad network.
  --save-code    Saves code text instead of execute it.

Read my manual on 'https://pad.programando.li/ruweb:manual'.

Executing example with my manual:}
    end
  end
end