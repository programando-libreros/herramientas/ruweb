# frozen_string_literal: true

require 'ruweb/sewmacros'

module RuWEB
  # Gets code
  module SewCode
    extend self
    
    def init(source, return_cmds: true, return_macros: false, return_both: false)
      @return_macros = return_macros
      @return_cmds   = return_cmds
      @return_both   = return_both
      @code          = sew(extract_blocks_from(source))
    end

    private

    def extract_blocks_from(markdown)
      b = '\u0060' * 3
      markdown.scan(/#{b}((.|\n)+?)#{b}/).map(&:first)
    end

    def sew(blocks)
      unless blocks.empty?
        blocks.map!.with_index do |block, i|
          lines = block.split("\n")
          if lines.any?
            meta  = lines[0]
            data  = lines[1..-1].join("\n")
            tokenize(meta, data, i)
          end
        end
        tipify(blocks)
      end
    end

    def tokenize(meta, data, index)
      case meta.split(/\s+/).length
      when 0 then nil
      when 1 then { index: index, cmd: meta[/^\w+/], data: data }
      else obtain_custom_or_macro_from(meta, data, index) end
    end

    def obtain_custom_or_macro_from(meta, data, index)
      obj    = { index: index }
      custom = meta.gsub(/^\S+\s+/, '')
      macro  = custom =~ /^\u005F/ ? custom : nil
      macro ? obj[:macro] = macro : obj[:custom] = custom
      obj.merge({ data: data })
    end

    def tipify(blocks)
      blocks.compact!
      cmds   = arrange(blocks)
      custom = rearrange(blocks)
      macros = preserve('macro', blocks).reverse
      cmds   = RuWEB::SewMacros.init(macros, cmds: custom.concat(cmds))
      resolve(macros, cmds.sort_by { |e| e[:index] })
    end

    def arrange(cmds)
      arranged = preserve('cmd', cmds).map! { |e| { cmd: e[:cmd], data: '' } }.uniq
      pick(cmds, arranged)
    end

    def pick(cmds, arranged)
      arranged.map! do |e1|
        picked = cmds.select { |e2| e1[:cmd] == e2[:cmd] }
        index  = picked.map { |e| e[:index] }.min
        data   = picked.map { |e| e[:data] }.join("\n")
        { index: index, cmd: e1[:cmd], data: data }
      end
    end

    def rearrange(custom)
      custom = preserve('custom', custom)
      custom.reject! { |e| e[:custom] == 'x' }
      custom.map { |e| { index: e[:index], cmd: e[:custom], data: e[:data] } }
    end

    def preserve(key, array)
      array.select { |e| e[key.to_sym] }
    end
        
    def resolve(macros, cmds)
      case
      when @return_both
        cmds.concat(macros)
      when @return_macros
        macros
      when @return_cmds
        cmds
      end    
    end
  end
end