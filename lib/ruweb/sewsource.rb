# frozen_string_literal: true

module RuWEB
  # Gets source
  module SewSource
    extend self
    
    def init(raw)
      @source = sew(remacro(raw))
    end

    private

    def sew(markdown)
      regex = /\u005F\[(.+?)\]\((.+?)\)/
      markdown = markdown.gsub(regex) do |match|
        name  = match.gsub(regex, '\1').strip
        uri   = match.gsub(regex, '\2').strip
        puts  "🕷️ ruweb: other pad detected: sewing '#{name}' from '#{uri}'"
        pad   = remove_metadata(RuWEB::Read.init(uri))
        generate_source_block_from(match[1..-1], pad)
      end
      markdown.scan(regex).any? ? sew(markdown) : markdown
    end
    
    def remacro(raw)
      tag  = /\S*=>\s+/
      stop = /[\.,:;!?]/
      code_tag = "\u0060" * 3
      raw.gsub(/\s+#{tag}.+?#{stop} */,) do | protomacro |
        macro = protomacro.strip.gsub(tag, '').gsub(stop, '').gsub(/\s+/, '_')
        lang  = protomacro.split('=>').first.strip
        lang  = lang.empty? ? 'ruby' : lang
        "\n\n#{code_tag}#{lang}\n_#{macro}\n#{code_tag}\n\n"
      end
    end

    def generate_source_block_from(link, pad)
      msg = "\n\n:::info\n▶️ [#{Time.now}] #{link}\n:::\n\n"
      "#{msg}#{pad}#{msg.gsub('▶️', '⏹️')}"
    end

    def remove_metadata(markdown)
      markdown.sub(/^---\n+(.|\n)+?---(\n+|$)/, '')
    end
  end
end