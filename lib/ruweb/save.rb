# frozen_string_literal: true

module RuWEB
  # Saves the content
  module Save
    extend self
    
    def init(file, content)
      content = File.extname(file) == '.md' ? content : convert_data_of(content)
      puts "🕷️ ruweb: parameter detected: saving '#{file}' in '#{Dir.pwd}'"
      File.write(file, content)
    end

    private

    def convert_data_of(content)
      content.map { |e| "$ #{e[:cmd]}\n#{numerate(e[:data])}" }.join("\n\n")
    end

    def numerate(data)
      data = data.split("\n")
      data.map!.with_index { |e, i| "#{format(i, data.length)}│ #{e}" }
      data.join("\n")
    end

    def format(num, total_num)
      num  += 1
      chars = total_num.to_s.length - num.to_s.length
      space = ' ' * chars
      "#{space}#{num}"
    end
  end
end